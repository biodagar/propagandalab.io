[<< Back to Home](http://biodagar.github.io/propaganda/) | [Bibliography](http://biodagar.github.io/propaganda/bibliography) | [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)

# References

This page collates all resources actively referenced in the pages of this site. 

<hr />

A.B. 2017. ‘The problem with scientific publishing: and how to fix it’, in *The Economist*. Online: <https://www.economist.com/the-economist-explains/2017/03/30/the-problem-with-scientific-publishing>. Last accessed 12 April 2019.

Johnson, Lissa. 2019. ‘The Psychology of Getting Julian Assange’ (series) in *The New Matilda*. Online: <https://newmatilda.com/2019/02/19/psychology-getting-julian-assange-part-1-whats-torture-got/>. Last accessed 12 April 2019.

Hall, Michael L and Bodenhamer, Bobby G. *Mind-Lines: Lines for Changing Minds*. Neuro-Semantic Publications: USA.

McFarlane, N. 2018. *Spinfluence: The hardcore propaganda manual*. 2nd Edition. Carpet Bombing Culture: London.

Welch, D. 2015. *Propaganda, power and persuasion*. I.B. Taurus: London/New York
