[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Questions](http://biodagar.github.io/propaganda/questions) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Posts](http://biodagar.github.io/propaganda/posts/PostsIndex)

# Elements of Propaganda

<hr />STATUS: UNDER CONSTRUCTION - IN PROGRESS

<hr />

What do the texts say about the elements of propaganda? What makes propaganda what it is, and effective? 

## Goal

To win hearts and minds, because public opinion is a critical factor in the formulation of government policies.[1] Achieving this requires **deep understanding** of the audience[1] and how to drive the Influence cycle.


## Style
**Contrast** is an element in propaganda that is important. David Welch argues that this is because contrasts contain 'greater emotional intensity', and they 'guide the audience's sympathies with more certainty. He points out that humans seek psychological certainty, particularly in times of crisis: They look for something to latch onto. The use of contrasts, effectively deployed, allows you to direct your audience in the direction you want them to go. One of the reasons is that propaganda is most effective in times of uncertainty.[1]

It is **simple**, concentrating on as few points as possible.[1]

It is **most effective when it’s the least noticeable**.[1]

## Relevance

In situations of ‘total war’, in which civilians are as much a part of the effort as any explicit recruit, propaganda **must square with everyday experiences** otherwise it will be ineffective. [1]

**You ‘don’t need facts per see, just the best facts put forward**.[1]

The propaganda must always **consider the culture** of the target market - and uses it to drive the Influence Cycle.

## Attitude

It is most effective when it **reinforces previously held opinions and beliefs**. That was stated by Jacques Ellul (1965). [1]

Propaganda **confirms rather than converts**. It does this by sharpening and focusing existing beliefs, and operating on many levels of truth.[1]

## Timing
**Times of uncertainty** are times when propaganda is most effective. [1]

It is a **“gradual seduction**”. Stretching the campaign out over weeks, years, or even decades, may be necessary to cause people to think the way you want them to think.[1]

It is **repeated many times** and paired with continuity and sustained uniformity in its application. This means. for example, that the same message, or phrase, is repeated in every channel in the news (print, radio, TV).

## Emotion
**Hatred unifies** more effectively, simply, and easily than any other emotion. This is why propaganda in uncertain times, with hatred as its goal, works so well: It unifies groups of people very quickly. Welch writes[1] that propaganda has its best chance of being successful when it 'clearly designates a target as the source of all misery or suffering'.

It must **appeal to both the rational and irrational mind**. [1]

**Fear** when used correctly will have a galvanizing and mobilising effect on a population, as opposed to a paralysing one.[3]

## Targets
All propaganda **identifies an 'enemy'** or 'other' of some kind, and leverages stereotypes towards which to funnel attention. According to David Welch,[1] this not only draws aggression and hatred from a population, but allows the propagandist to direct attention away from other issues or 'genuine social and political problems at home'.

## Coherence among channels
Propaganda messages are **coherent through all channels**, which are used individually and collectively for the same message, in the same period of time, on repeat.

## Channels

* Posters and visuals[1,2]
* Calendars (in which dates of atrocities are circled)[2]
* Film, both ‘official’ propaganda and ‘unofficial’ propaganda (eg in War situation)[2]
* Radio, especially when in one nation but controlled by another.[4]

##  References

[1] Welch, D. ‘Introduction’ in *Propaganda, Power and Persuasion: From World War I to Wikileaks*. I.B. Taurus: London.

[2] Richards, Jeffey. ‘George Arliss: The superstar as propagandist. British propaganda in the interwar period’, in Welch, D. (ed)  *Propaganda, Power and Persuasion: From World War I to Wikileaks*. I.B. Taurus: London.

[3] Welch, D. ‘Today, Germany, Tomorrow the World’, in *Propaganda, Power and Persuasion: From World War I to Wikileaks*. I.B. Taurus: London. p106

[4] Rawnsley, Gary D. ‘Radio Free Asia and China’s Harmonious Society’, in *Propaganda, Power and Persuasion: From World War I to Wikileaks*. I.B. Taurus: London. p139