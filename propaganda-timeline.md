[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Elements](http://biodagar.github.io/elements/) | [Issues](http://biodagar.github.io/propaganda/issues) | [Questions](http://biodagar.github.io/propaganda/questions)

# Propaganda Timeline

This timeline is in lieu of a history of propaganda. 

While the history is important (see Question 4.1), because the shifting sands of time show us how the perception and tools of propaganda have changed or developed, tracing a full history is irrelevant to this project. What *is* relevant, however, is a timeline; and then only for reference.

**This timeline is dramatically incomplete.** It is a living document, drawn from current and past readings only. It’s important to understand that this project’s goal is not to trace a definitive timeline, but to trace only those moments pertinent to this particular project.

***

1950	First year of NATO alliance; NATO is engaged in the projection of information.

1982	Ministry of Defence and Royal Navy resolve to manage and manipulate the media in the Falklands War

1985	Press denied access to the Granada operations in the Gulf

1980s: Late	Gulf War is considered the first ‘Information War’

1989	Press denied access to the Panama operations in the Gulf

1991	USA’s 4th Psychological Operations Group deployed into the Gulf War

1992	Establishment of the World Wide Web

1999	NATO exaggerated and lied about Serbia during the Kosovo conflict, which made the public wary