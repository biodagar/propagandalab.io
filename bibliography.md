[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)

# Bibliography
A living document for all of the titles about propaganda that I have found, been sent, or had recommended. See [the objectives](https://biodagar.github.io/propaganda/objectives) for why this page is important. You’ll find works actively referenced on this site in [references](references).

This page is in alpha order by title.

**KEY**

[R] = Read

[P] = In Progress

If you know something is missing, [view this site on Github](https://github.com/Biodagar/propaganda) > Issues > Create an issue to let me know. Alternatively, you can [send an email](mailto:biodagar@bioagar.com).

<hr />

[R] *1984*, by George Orwell

*Dictators' propaganda*, by Aldous Huxley

*the Fate of Empires and Search for Survival*, by Sir John Glubb

[R] *Gulag Archipelago, The*, by Aleksandr Solzhenitsyn

*Information war: American propaganda, free speech and opinion control*, by Nancy Snow

*Justifying war: Propaganda, politics and the modern age*, by Welch & Fox

*Manufacturing consent* by Noam Chomsky

*Media Control* by Noam Chomsky

*Mesmerization* by Gee Thomson

[R] *Mind-Lines: Lines for Changing Minds* by L. Michael Hall and Bobby G. Bodenhamer

*Munitions of the mind*, by Philip M Taylor

*Phantom public, the*, by Walter Lippmann

*Propaganda*, by Edward Bernays

*Propaganda and mass persuasion: A historical encyclopedia* by Cull, Culbert & Welch

[P] *Propaganda, power and persuasion*, edited by David Welch. 

*Propaganda Inc.: Selling America's culture to the world*, by Nancy Snow

*Propaganda techniques in the World War*, by Harold D. Lasswell

*Propaganda: The formation of men's attitudes*, by Jaques Ellul

*the Psychology of Getting Julian Assange, Parts 1-5*, by Dr Lissa Johnson

*Public opinion*, by Walter Lippmann

*Public Relations*, by Edward Bernays

‘Thought Crime’ in *The Guardian* by Kathleen Taylor 

### Contributors
Thanks to the following folk for their input thus far: Chris H, Alex L.



<hr />

## Changelog

| Created      | Updated       | Changelog                                                    |
| ------------ | ------------- | ------------------------------------------------------------ |
| 7 April 2019 | 12 April 2019 | Add link to references. Add key. Add key elements to text. Amend format for clarity. |
|              |               |                                                              |

