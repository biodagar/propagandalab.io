In *Spinfluence* the argument runs that the method is such:

1. Create an idea to capture as many hearts and minds as possible
   1. Mobilise against “an enemy”
   2. Retain happiness “spending”
   3. Create “leadership”
   4. The ideas feed objectives. Those objectives are: “Control the narrative, around the interest of a specific group; nullify unhelpful ideas; agitate or desensitise the emotional state of the target audience”.
2. Understand your target audience as deeply as possible: hopes, fears, dreams, desires. [Copywriting 101] Understand social, geographic and economic demographics. Understand its heart and motivations for behaviours.
   1. Your target audience needs to be one that is highly influential, and capable of shifting the narrative of the broader population. Think: Journalists, mums, GPs, teachers, employers, retailers.
   2. Evaluate a situation by putting feelers out, assessing, and adjusting, and retrying
   3. Run a pilot to see if it ‘takes’ and how to better understand your audience. [Customer Validation]
   4. Zooming out to a knowledge of the big picture allows you to create a framework at detail level. Identify ‘social faultlines and hotspots’. Tap into the zeitgeist.
3. Create language (slogans and sound bites) to deliver **pervasive** messages [programming via subconscious]
   1. Ensure that it creates a contextual framework
   2. Ensure that the framework is robust from multiple directions
      1. Delete or omit information and data [NLP], eg censorship, ownership, authorship, etc.
      2. Distort the events, eg by highlighting trivialities rather than actual information. [NLP]
      3. Link concepts together, so that they trigger each other [NLP]. ‘Propaganda triggers emotions by activating subconscious networks of association’. This is the meaning making link to past behaviours and experiences.
      4. Repeat it. Everywhere.
      5. Understand and create particular emotions: inspiration, fear, paranoia, 
      6. Use abstractions so that there is enough ambiguity that when you make meaning it is in whatever direction you are pushed. Create the meta, meta-meta, and meta-meta-meta level contexts to shift the frame, which will make the action invisible. See also use of euphemisms (abstraction of the VAK-V level), and use of cultural or citizen values.
   3. Symbols are important because ‘they are easily understood’. *Spinfluence* doesn’t present the idea that symbols are understood by the subconscious mind more than by the conscious mind, and that it is therefore a faster way to program a population. This ensures that you can bypass the brain ‘on route to the heart’. Emotions make decisions.
      1. Rational argument will collapse ‘under the weight of emotional insecurity’ - and therefore make rash decisions. ‘Emotions are like shortcuts, allowing people to make instant decisions’.
   4. Establish a series of triggers. The triggers create the desired behaviour. ‘Triggers can be as simplistic as a word, colour, images, smell or tone of voice’. [Mind control]  This is why fear is used: People will rush away from the fear without examining whether the fear is valid. Desire’s power is doubled when used in the context of fear.
   5. Repetition of symbols and triggers will cause populations to act as if under a spell
4. Create visuals to put where words fail [programming via subconscious]
5. Take action in ways that make the words and visuals significant
6. Use the tools of propaganda to reinforce points 2-4
   1. Make sure that all channels and points of transmission *function as one*
   2. Traditional media gives you reach
   3. Online media gives you simpler access to emotional triggers and to the contagion of memes (“go viral”)
   4. Be blunt that there is a bandwagon you can join or miss out on.
   5. Use testimonials.
   6. Rent a crowd, to *look* like support pre-exists. This supports human biases of going with the crowd.
   7. Distribute and reinforce falsity.
   8. Start Rumours.
   9. Conduct character assassination.
   10. People will swallow everything labelled as ‘news’, despite the corporatism behind it.
       1. These are advertising-funded. Therefore, anything that upsets advertisers won’t be run.
       2. These are often filled by government press releases: Themselves a standardised filter.
   11. opinion management
7. Points 1-5 enable whole populations to react/behave in the same way. This is herd behaviour. People submit to it to minimise their own risk. Threats may be physical or emotionl, which explains the primitive fight-or-flight responses. This is what creates ideologies. People outside the ideologies are left to fend for themselves. The resulting behaviour becomes self-perpetuating.
8. WOLF = warden of language and falsities = the person who creates the frame
   COWS = corporation owned wage slaves = the people inside the frame. 
9. The art of propaganda, when wielded correctly, ‘…creates “a form of group-psychosis” in which peer pressure and a fear of being isolated sweeps all cows away individually while binding them together in a form of mass hyseria.’

*Spinfluence* leans heavily on the idea of manufacturing consent, first posited by Noam Chomsky. Which again begs the question: Why do people not question that this is the point? Why do they rely on Chomsky’s work as the goal? Why ‘manufacture consent’ and not ‘programming’?

References to chase down:

* Walter Lipmann’s works on persuasion and propaganda
* Noam Chomsky: Why does everything come back to this point in the funnel? Refs include *Media Control* and *Manufacturing Consent*
* Edward Bernays *Propaganda*
* Gee Thomson, *Mesmerization*
* Kathleen Taylor, ‘Thought Crime’ in *The Guardian*