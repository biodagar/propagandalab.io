[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Questions]((http://biodagar.github.io/propaganda/)) | [Definitions](http://biodagar.github.io/propaganda/definitions) 

# Issues

[TOC]



## 1. Propaganda has different definitions.

James Chapman writes that there are several perspectives about propaganda, that it is no longer a dirty term, and is now considered worthy of study. He points out that the negative definition of propaganda emerged as a result of the wars. He writes:

‘The idea that propaganda was inherently a bad thing was summed up by American social scientist L. W. Doob in 1935 when he wrote that “the word propaganda has a bad odour. It is associated with war and other evil practices”. Another misconception - largely arising from that “bad odour” - was the view that propaganda had no proper place in liberal democracies.’(Champan J, in Welch 2015,p79). He goes on to say that there are other misconceptions: That it doesn’t have a place in Britain, despite the fact that ‘Britain was one of the first nations to recognise the need for an official agency to project the national case’, not only during war, but also in periods of what he terms ‘international struggle’.

At a cultural level, propaganda may be defined any production that provides ‘social or political comment’. (Chapman, in Welch 2015, p86).

## 2. Propaganda is negative, as with its sister term ‘manipulation’.

**David Welch disagrees**. In his 2015 volume, *Propaganda, power and persuasion: From World War I to Wikileaks*, he puts ‘propaganda’ in neutral territory: ‘Propaganda is ethically neutral - it can be good or bad’.

## 3. Propaganda is a specific tactic or method, separate from other types of persuasion or manipulation.

**David Welch demonstrates this** in his foreword to *Propaganda, power and persuasion*,(2015) in which he illustrates how terminology for propaganda has shifted over time. He traces the shifting terminology from *propaganda* to *information warfare*, to *information operations*, to *psychological operations (psyops)*, to *perception management*. 

It’s one of the key reasons why, for this project, I have [defined ‘propaganda’](../definitions/propaganda-definition).

James Chapman talks about propaganda as a defined activity, in his examination of the formation and activities of Britain’s Ministry of Information, and in his explanations of ministerial in-fighting over who gets the power to do it. (Welch, D. 2015. p83)

The emphasis on departments and ministries may be one reason why propaganda is seen as a siloed activity. However, Nicholas J. Cull, in his paper ‘The Tragedy of American Public Diplomacy 1989-1999’ in Welch (2015), shows that the messages of a nation, and how it functions, are fundamentally separate from a decisive propagandist perspective. He demonstrates that the US Information Agency (USIA) folded to become the Department of State, and that in the shift out of Cold War and engagements with Russia, to engagements in the Middle East, the narratives had to change. They dropped the reliance on faith-based messages, and replaced them with capitalism-based messages. He also talks about the narrative being handled by commercial agencies rather than government-controlled agencies, and that branding was important. But along the way, the issue of *where is the propaganda* slips out of the author’s fingertips. Instead of it becoming an issue of propaganda, it becomes an issue of ‘public diplomacy’. 

## 4. Journalism is propaganda.

In his examination of Radio Free Asia, Gary D. Rawnsley suggests that news can’t be considered to be separate from politics simply because of the biases inherent in the radio station’s perspective. He writes: ‘Such a separation conveniently ignores not only the inherent bias in RFA’s programming, but also the propaganda value of news and the usefulness of maintaining a commitment to professional journalism: Is news, as Lord John Reith noted, really ‘the shocktroops of propaganda’? ’ (Rawnsley in Welch 2015, p134)

## 5. Propaganda is the artful execution of neuro-linguistic programming.

Nicholas McFarlane’s book *Spinfluence* outlines a methodology and function of propaganda for the purposes of population behaviour and attitude control, which includes aspects of neuro-linguistic programming and neuro-semantics, as covered by Hall and Bodenhamer in *Mind-Lines: Magical Lines to Transform Minds*. In particular, he includes the techniques of deleting or omitting information, distorting information, linking concepts together so they trigger particular responses and behaviours, and the use of symbols, stating that symbols are important because ‘they are easily understood’.

While *Mind-Lines* isn’t a book on propaganda at all, but instead an introductory work on neuro-linguistic programming and neuro-semantics, it articulates a process of ‘setting the frame’ and of changing the frame to create changes in attitudes and behaviour. At its heart, this is the objective of propaganda. Hall and Bodenhamer point out that the multiple levels of context (meta-level, in which you create meaning about what you see, hear, feel; meta-meta level, in which you think about the meaning you create; and meta-meta-meta level, in which a broader context reorients your thinking to be a particular thing) are what will shift thoughts and feelings, even about extreme phobias, extremely quickly. As they point out, ‘Frames govern meaning. … frames govern, modulate, organize, drive, and control the experiences that occur within them (i.e. the thoughts, feelings, language, behaviour, and responses). When we set a frame, that frame will govern the consequences and conclusions that follow. Korzybski called this, “logical fate”… He who sets the frame governs the experience’. (Hall & Bodenhamer, p198)