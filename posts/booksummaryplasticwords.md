# Book Summary: Plastic Words, by Uwe Poerksen

Title: *Plastic Words: The Tyranny of a Modular Language*

Author: Uwe Poerksen

First published in Germany as *Plastikworter: Die Sprache einer internationalen Diktatur* in 1988 by Klett-Cotta. This summary is of the English translation from 1995, by Jutta Mason and David Cayley, which was published by The Pennsylvania State University Press.

ISBN: 0-271-01476-8

## Preface to the English Translation

The Preface to the English Translation is a word from the author about the evolution of the title and its introduction to audiences in its Englis form. The preface gives you some context for the concept of plastic words. Drawing on other authors, such as George Orwell and Erhard Eppler, Poerksen talks about how the concept of synthetic words and the role that language plays in establishing models of reality suddenly became part of public and political discussion. He suggests that the international nature of plastic words is 'not merely amusing', and provides examples from German political history and how social discourse was intentionally shaped and changed after the Berlin Wall came down. 

As the author writes, 'This book is neither pessimistic nor optimistic; it wants to say, "Stop transmitting messages of NOTHING".' His argument is that people need time space in which to think, to debate, to consider. And that plastic words do not allow us the luxury of this space. 

## Preface

The original preface is, unlike the foregoing, a primer for understanding the  remainder of the essay, particularly in relation to two things: The separation of colloquial and common 'vernacular' language from the sciences; and what he actually means by mathematization of language. By 'mathematization of the vernacular' Poerksen refers to what he describes as 'the mingling of spheres': The movement into everyday language of scientific concepts, elements and principles, and how he discovered the ways in which the realms cross over. Poerksen further explains how science, when it becomes vernacular, becomes contradictory, doctrinaire, and imperalistic. Rather than covering up the problems created by the movement of terms between spheres and disciplines by using 'pseudo', Poerksen chose to focus on clarifying the connection between how words are used, abused, and transformed when they are used in each realm. 

## Introduction

Poerksen opens the book by pointing out that plastic words are not new, but the usage of them is new; that their usage is what has formulated, and is what is driving, a new type of civilization. Right in the first paragraph of the introduction, you are introduced to the concept of these words appearing simultaneously in many different places, as though the release and use of the words has been planned like an assembly line. 

The pattern of plastic words is that they become fashionable, they command attention, and then they disappear into every day speech and *appear* to become commonsense. 

The author then describes how the concept occurred to him, when he was at a conference in Mexico, listening to speaker who, he suggested, used no more than a hundred words. The person to whom he made the comment rebuked him by suggesting that 100 words would make you a leader, but that the speaker at the conference used perhaps only fifty words. 

Poerksen runs through the nature of languages around the world, and suggests that there is a pattern that describes how nationhood destroys languages. 'The nation state weeds out languages,' he writes, pointing out that it is 'the salesman of global unification'. The models that developing countries follow are European, internationalised, promoted by such organisations as the United Nations. In striving to achieve a 'national identity', countries radically simplify the languages spoken and used within their bounds. 

Yet, even though the nation state destroys languages, even the nation state is not at the top of the pyramid. The top of the pyramid is dominated by a 'spreading international vocabulary' of a small cluster of words that appear unremarkable. The author describes them as 'the master key to the everyday':

'They are handy, and they open doors to enormous rooms. They infiltrate entire fields of reality, and they reorder that reality in their own image'.

That statement summarizes the nature of plastic words, and much of the rest of what follows.

The words are plastic because their meanings have changed. And yet, the change has been imperceptible. Many of the changes are recent: The shift in 'information' is within living memory. The author describes his previous work in this field, that it is possible to study the present accurately, but only if one is clear about separating the science meaning from the everyday meaning of words that science absorbed from folk languages after it abandoned Latin.

Explaining his work, the author describes the theme as one of the destruction of the vernacular. That, as science takes over everyday language, it makes political systems irrelevant, it paves the way for industrial operations on a grand scale. He suggests that, while it's easy to make words the scapegoat, they are the structure that makes everything else visible. He suggests that language isn't just a tool for making things visible; rather, it is an agent of change in and of itself. 

'Language is a partially autonomous power that shapes reality as well as reflecting it, and these two functions interact.'

Poerksen describes how, when the plastic words are used as described in the chapters that follow, the words form not just a module of a new reality; but that it is a 'reality that locks us into a conceptual prison'. It isn't just the words themselves; it is the full linguistic application of those words. That they have a grammar, a form, and a function, and that it something you have to be aware of lest you effectively turn into a linguistic wanker.

The author's view of language's agency isn't so much about the romantic vision of the Volksgeist or spirit of the people, but an institutionalising tool that sanctions social and historical practice. It takes on this function because it is the one thing jammed between you and the world. It solidifies systems by entrenching ideas and influence. 

Words that are plastic consist of many meanings, from many disciplines, fields, industries, all bolted one on top of the other. They change shape depending on where they are used, and what their environments are like. They have meanings that exist within them, but no image, sound, shape, or texture, nothing graspable. The objects that plastic words refer to have long since sunk below the surface, and all we have left are the ripples left behind. In this analogy, which appears in the book, the denotation (referent, or object) is a stone thrown into a pond. The ripples outwards from there (the feelings, associations, images, valuations, evokations) are the connotations. Most words have a referent or image, something you could pantomime ('lamp', 'anger') but plastic words do not. Their rocks have long since sunk away, and the only things they leave behind are the rings of connotation.

While everyday words can also be abstract or vague, if you use them in a specific context, they assume a specific form and meaning. They become precise. Plastic words don't do this. They are rarely used in particular or precise ways; instead, they are always abstract, general, and vague. The richness of contexts and nuance is what gives langauges their ability to represent human experiences. But plastic words eliminate that richness. The author describes all of the different types of love, each instance different, shaded or coloured in a different way. But 'communication' or 'sexuality' isn't like that. In a sense, the words become categories, rather than nodules of meaning.

Towards the end of the introduction is a summary of the chapters in the book and how they relate to each other. The atuhor points out that the rigour of analysis that he uses isn't a parody: Instead, he found no other way of approaching the phenomenon.



## Chapter 1: Plastic words on both sides of the Elbe

In the first part of this chapter, Poerksen examines two words, which were used in the very different societies of East and West Germany. He takes two words—*sexuality* and *development*. The first in the west; the second in the east. His examination takes both words to the abstract level and demonstrates their similarities. 

The term *sexuality* had arrived in everyday language 