[<< Back to Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Home](http://biodagar.github.io/propaganda/)

# On Objectivity

Despite this project having only just gotten underway, it is already abundantly clear that the very subject of *propaganda* is fraught with danger. 

The danger lies in the complete acceptance of a dominant framework, which authors tend to accept. As one writer whom I read recently (and which, I realise now, is poking holes in how robust my own project actually is, thanks to my inability to reference it) said, American writers think in an American way; British writers think in a British way.

It is, however, more significant than this. The significance is a generic acceptance of a dominant, emotional narrative - such as, the anti-Donald Trump narrative surrounding his election to the USA Presidency in 2016 - that then finds its way, unquestioned, into texts that are apparently designed to question the status quo. 

How robust, then, are the methods that the writers present?

This question reveals further notions that are themselves disturbing, such as: How many of the known works on the subject of propaganda are themselves works of propaganda? How meta does one need to become?

Approaching the subject with syntopical eyes ought to allow me to spot the rogues, because doing so will:

(a) force the authors to come to terms with my framework

(b) force me to question everything about each one.

There is, of course, the argument that no reading can ever be truly objective. However, as far as addressing this concern—if you, or anyone else reading this, has that question—all I can say is that the syntopical reading methodology creates distance. Distance is as far from the subject as I can hope to achieve. It’s for this reason that syntopical reading of such a field as what is commonly known as ‘propaganda’ is vital.

The questions developed throughout this study will be kept in the [Questions](http://biodagar.github.io/propaganda/questions) file.

<hr />

## Changelog

| Created       | Updated       | Changelog                                            |
| ------------- | ------------- | ---------------------------------------------------- |
| 11 April 2019 | 11 April 2019 | Fix minor edits for sense. Add line above Changelog. |
|               |               |                                                      |

