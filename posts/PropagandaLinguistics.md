[<< Back to Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Home](http://biodagar.github.io/propaganda/)

# Propaganda is programming

This post attempts to start answering [Question 6](questions).

I’ve begun to form the view that neurolinguistic programming and neurosemantics are critical to an understanding of how propaganda works. No, more than this; my view is much stronger. In fact, I’m beginning to wonder if the focus on propaganda as ‘action’ has served to obscure the fact that ‘propaganda’ is the action of programming populations in a particular way. 

Given much of this occurs through language, given language is merely a symbol, and given the subconscious mind thinks and acts on symbols, incorporating the study of neurolinguistic programming and neurosemantics becomes not-negotiable when compiling a syntopia about propaganda.

## Defining ‘propaganda’ 

You will recall from the [Definitions](https://biodagar.github.io/propaganda/definitions/propaganda-definition.md) that I am following a specific definition of the term ‘propaganda’. That definition is:

> Propaganda is a sociocultural meta-level concept, which describes the definition and systematic propagation of a set of doctrines, ideas, or principles of any individual, organisation, or movement (or collection thereof), disseminated through ideas, information, public actions, covert actions, displays, influence, behaviours and commentary (among other things), for the purposes of shaping opinions, perceptions, and ultimately the actions of individuals, in support of (and to further) the purpose of the doctrine(s).

For the purposes of the present post, we are going to look at some key notions from that definition. These are:

that propaganda is

* a sociocultural, meta-level concept

that works on

* ideas or principles

in order to

* shape opinions and beliefs in favour of an outcome.

It turns out that this maps very nicely to [Hall and Bodenhamer’s](http://biodagar.github.io/propaganda/references) model of neurolinguistic programming (NLP), which tells us that in order to change or create a belief, you need to change that person’s contexts or frames for how they connect events or behaviours to their own ideas about those things. The authors provide a means for unpacking the ways in which this occurs, in every day conversations, in speeches, in media of all kinds - from television to books.

They also point out that at its core, NLP is really about mind control.

So, too, is propaganda. Isn’t it?

## Scratching the surface of NLP and neurosemantics

> ‘Whether in the structure or system of families, businesses, churches, schools, political parties, or any social environment, we have to use language. And language can create or destroy, can enhance or limit the “life” of a system.’ - Hall and Bodehamer

To truly see the function of NLP and neurosemantics in play in propaganda, I am going to have to spend some time in deconstruction mode, to demonstrate how it functions. It’s possible that many others have already walked this land ahead of me, and that I haven’t included their works yet. Right now, I’ve barely scratched the surface.

Some things that I haven’t done include applying [Hall and Bodenhamer’s framework](http://biodagar.github.io/propaganda/references) to any type of media. I haven’t read any other books - not even Noam Chomsky’s, whose Transformational Grammar was, according to Hall and Bodenhamer, one of the key elements that sparked the NLP revolution, as it were. I also haven’t used the framework proactively, such as in creating story through metaphor to demonstrate how the application of the method functions to change belief. 

There is much more work to be done.

### So far: Understanding how contexts are created and how to use them

However, in unpacking the function of propaganda one must go far beyond the surface levels of media, repetition, and use of images/enemies/emotion, and go right down to the heart of what propaganda seeks to do. If it truly seeks to ‘change hearts and minds’, then what it is looking to do is create beliefs. What NLP tells us so prettily is that once a single meaning is created—let’s say, a causative action; X leads to Y—you can build on it through the use of abstraction. If you are able to control layers of abstraction, then you not only create meaning, you create assumptions, presuppositions, and nominalizations that are incredibly complex to unwind. The greater the level of abstraction—and Hall and Bodenhamer specify in their work at least three levels of abstraction—the more work it takes to unwind a belief.

It’s not to say that unwinding a belief is not impossible; far from it. It’s much more to say that *interrupting* or *changing* a belief requires an understanding of the entire structure.

In the NLP model proposed by Hall and Bodenhamer in [*Mind-Lines*](http://biodagar.github.io/propaganda/references), here is how humans create beliefs in which they function:

PRIMARY LEVEL			See, hear, feel sensations, say words		<<< This is your actual experience.

META LEVEL					Sets the context for the experience.

META-META LEVEL		Sets a frame-of-reference (meaning about) your existing frame << how you think about it

META-META-META LEVEL	Reorients you: ‘Not this’ but ‘this higher frame’

At each level, you are capable of unwinding the complexity, if you are armed with the right set of questions.

Crucially, however, the authors state:

> He who sets the frame (frame of reference) governs (or controls) the resultant experience (emotions, thoughts, responses).

Therefore, if I as an author of a propaganda syntopia, zoom right out from the models and actions and media (etc), then what we see is a rather simple construct.

That construct is:

If I control the context, I control what you think about an experience. Therefore, if I am a publisher, a news outlet, a person writing an algorithm that governs what appears in a social media feed, a politician, a journalist, a writer, a business person who controls what people see in relation to anything, in fact, then I am a powerful person.

Therefore, if you have this power, then your power exists in your ability to:

* Delete, omit, overlook, forget information when presenting it to someone else. This allows you to reduce the important awareness that other people have in relation to a situation of any kind. If they don’t have the information, but assume (because, for example, they trust you) that your information is complete, they won’t notice and may not ask for additional details. 
* Construct rules, generalise, standardise, identify patterns in information when presenting it to someone else. This allows you to remove details that will allow someone else to see the full picture and construct meaning different from what you’d like them to think.
* Distort information by adding or altering details. This allows you to turn processes into *things*, which is the land in which ideas become confused with reality. 
* Turn verbs into nouns, a process known in linguistics as *nominalization*. This freeze-frames an action and makes it static by labelling it, naming it, and treating it as a reified thing (which means to treat an abstraction as a real thing).

There are some ways in which you can spot patterns in propaganda; and that theoretically means that you can use the same methods to create your own propagandist environment. This is particularly the case once you realise that meaning is created in your own mind. It doesn’t *exist*; it doesn’t *exist anywhere in the world*. It’s just *inside your own head*.

You can facilitate this meaning-making activity, by creating a deep structure represented by (for example) the sentences you create. You would do this by using:

* Causation statements (*this A leads to this C*)
* Equation statements (*this A is this C*)
* Value words and ideas (*good/bad; important/unimportant; nice; ugly; etc*)
* Identification statements (*this A means this C about me*)
* Presuppositions (*unquestioned assumptions*)
* Nominalizations (*verbs turned into nouns*)
* Modal operators (*necessity, desire, possible/impossible, choice, etc*)

> ‘… once we learn how to assume ownership over this process, then we can truly choose the frames we want to live in and take charge of our emotional responses. This also protects us from those who might otherwise “set the frame”, thereby inducing us into feeling “manipulated” by their “mind control”. Mind-lines occur all around us and from every media (newspapers, television, books, speeches, everyday conversations, sales pitches etc).’ - Hall and Bodenhamer

## The question is: Do NLP and neurosemantics have a role to play?

Simply by reading [Hall and Bodenhamer’s work](http://biodagar.github.io/propaganda/references), my initial response is *absolutely*. 

How people think about the world, and modifying it, changing it, shaping it, is what propaganda does. You can’t do that unless you have a fine grasp of not just the linguistic and grammatical structures of language, but also how language is perceived and used *for the purposes of* creating or destroying beliefs.

### Where to from here?

Step one from here is verification. The main question to answer is whether or not Hall and Bodenhamer’s work is an accepted or challenged work—which is something I don’t know. If their models for describing both NLP and neurosemantics are critical path models, then I can move onwards. 

What will be interesting will be discovering:

1. How the models are regarded by other authorities on the subject
2. Who has already linked NLP, neurosemantics, and cognitive linguistics to propaganda (or created frameworks using it, or unpacked complete campaigns using these approaches)
3. Whether or not others have expanded on this work since it was written and, if so, what directions they took. 

Lots of thinking. Expect this page to be updated as reading progresses.

<hr />

## Changelog

| Date          | By      | Comments    |
| ------------- | ------- | ----------- |
| 25 April 2019 | Leticia | Create file |
|               |         |             |