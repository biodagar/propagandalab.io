[<< Back to Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Home](http://biodagar.github.io/propaganda/)

# On Propaganda Studies As Propaganda

This post attempts to start answering [Question 2](questions).

It is feasible that works on propaganda themselves form part of a propagandist’s work. 

## Origins of the notion

The idea struck me while in the very **first** chapter of the very first book I started reading when I decided to take this study seriously. That work - *Propaganda, power and persuasion*, edited by David Welch - has a seriously British perspective. That might not be a problem, and the history presented by Welch may well be accurate. But as a curious Australian, who tends to mistrust any source, my first question became: Yes, but is the Britain-centric perspective of the history of propaganda even accurate? Every one of the contributors to the volume are working inside one of the ‘Allies’ or ‘Five Eyes’ nations (but only the UK and the USA are represented).

The **second** time it occurred to me was when I began reading Nick McFarlane’s *Spinfluence* (the Fake News Special Edition). In McFarlane’s book, there not only appears to be an assumption that the dominant paradigm pedalled by the major press is accurate: 

‘Trump has proven to be a master of the sound bite sized, tweet ready, fact-free verbal insult.’

Given his statements are starting to be verified, since the Mueller Report, the veracity of McFarlane’s text needs to be questioned. 

The other issue that I have with McFarlane’s manual is that it doesn’t define the phrase ‘fake news’; instead, it takes the phrase at face value (that is, that it means ‘the news is fake’). I suggest that, while quotes pulled by McFarlane claim that there is no definition, it’s simply because nobody has thought about it enough. For the purposes of this project, I am following [a specific definition of the phrase *Fake News*](../definitions/FakeNews).

The **third** time it occurred to me was when I was scanning through my social media feed. I began to follow an account called [Media Lens](https://twitter.com/medialens), on the recommendation of Dr Lissa Johnson, whose analysis of [*The Psychology of Getting Julian Assange*](<https://newmatilda.com/2019/02/19/psychology-getting-julian-assange-part-1-whats-torture-got/>) was insightful. Media Lens states that it is ‘Correcting for the distorted vision of the corporate media’.

Aside from the point that Media Lens’s statement already assumes that the corporate media’s perspective is distorted, distortion always depends on what you’re looking at, through what lens, and from what perspective. 

I discovered that Media Lens simply accepts a dominant narrative that gets you *flayed in public* if you run against it: Climate change:

![](../images/20190411-MediaLensShare-Climate.png)

## Verification of the question

One of the hallmarks of propaganda is that it [supports and reinforces](elements) a particular doctrine. In our current society, just asking a question about climate change is enough to have you slaughtered by your peers, and your reputation called into question. This occurs even when these people are aware of the [problems with scientific publishing](<https://www.economist.com/the-economist-explains/2017/03/30/the-problem-with-scientific-publishing>). 

**Three times and you’re out?** 

If nothing else, these occurrences, one after the other, have at least verified the need to ask the question. 

For example: Being able to control a meta narrative enables you to capture even the curious, questioning people, if you are driving a doctrinal line for some reason, right? 

This is why maintaining a view that questions every aspect of a society’s cultural attitudes is important. As Welch points out, propaganda is ethically neutral. It is a tool, it is a necessary part of the rhetorical toolkit to which everyone who need to persuade others has access. It doesn’t necessarily mean that you are misleading people in order to do it. But if you can create frameworks of study that then guide other students, and those frameworks are considered definitive, then I suggest that you have an even greater responsibility to ensure that you are walking a very uncomfortable line, and must be willing to be vilified by people who disagree.

So far, all I have are sub-questions. Whether or not these questions themselves form part of a propaganda’s toolkit is another question in and of itself. 

Further thinking and analysis on this topic will follow.





<hr />

## Changelog

| Created       | Updated | Changelog   |
| ------------- | ------- | ----------- |
| 12 April 2019 |         | Create file |
|               |         |             |

