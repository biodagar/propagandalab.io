# The Neuro-Semantic Experiment: How are the propaganda patterns working?

Because I can’t stop thinking about the impact of neuro-semantics in the role of programming minds, I propose an experiment to see if patterns of propaganda become evident.

Consider this post to be the notes from the experiment.

## The hypothesis

The hypothesis for this experiment is that the statements of influential Americans with a known right or left political alignment can be understood—and the propaganda patterns revealed—using a methodology derived from neuro-semantics (itself derived from neuro-linguistic programming).

## The methodology

The method involves selection of participants, the analytical method, and the application.

### The analytical method

The analytical method chosen for this experiment is a derivative of that outlined by Hall and Bodenhamer in [*Mind Lines*]((http://biodagar.github.io/propaganda/references). The method is multi-layered.

Analytical pass #1: Notice *causation*, *equation*, and *universal quantifiers*. We are looking for *because, if, when, so that*; *is, equals, equates to*; *all, always, never*. Additionally, we’re looking for all present-tense verbs.

Analytics pass #2: Enquire about *specifics*, which we’ll discover in the See, Hear, Feel, Smell, Taste expressions and behaviours.

To identify **beliefs** the method asks us also to examine *value words and ideas*; and presuppositions, and unquestioned assumptions.

To do this, we need to ask some specific questions of whatever text is in front of us. Those questions include:

* What are the components, and which ones of them are VAK-OG? (primary experiences of seeing, hearing, feeling/emotions, olfaction, and gustation)?
  * * In what order do they occur?
    * What framing exists and how is it being used?
* What is excluded?
* What representational signals do the words imply?
* What *Yes!* affirmations does it pre-suppose about an idea?
* What frames of reference do the words imply?
* What operational beliefs appear to be driving the assumptions?
* For this to make sense, what must the person assume to be real?
* What perspective is governing the process?

### Application of the analytical method

The analytical method will be applied to written text in the first instance. Each snippet or section of writing will be analysed piecemeal: Sentence by sentence, and then also as a whole.

If the outcomes prove to be promising, the method will be applied to other media, chiefly interviews or videos of each person selected.

### Selection of subjects

The subjects chosen for this experiment were selected by a futurist who keeps close watch on US politics, and is therefore familiar with the politically aligned positions of each subject. The subjects have been categorised according to this third party’s assessment of where they sit.

For the purposes of this first, lightweight experiment, I haven’t sought to verify the categorisation provided by the third party.

In order to effectively assess the frames working in the background, a central issue has been chosen. This central issue—‘Donald Trump as President or President-elect’—allows two things: First, a political focus that will necessarily force a divergence in beliefs; second, a clear-cut issue that will see each subject attempt to persuade its audience in a particular direction; third, a more streamlined and effective discovery phase.

The criteria for inclusion, of a statement by each subject, are:

* it’s directly from the subject, and as such counts as primary data
* it mentions Trump in some way
* it forms an apparently obvious personal perspective, opinion, or story.

The primary channel sought for statements is social media, primarily Twitter. Where a subject or its direct postscan’t be located in social media, articles or blogs written by the person (e.g. on Medium) will be sought. Thereafter, transcripts of audio (from podcasts, for example) will be used. 

In the interests of brevity, two samples from each subject will be analysed.

#### The subjects

The subjects chosen for inclusion are:

| Right-aligned      | Left-aligned          |
| ------------------ | --------------------- |
| Paul Joseph Watson | Krassenstein Brothers |
| Alex Jones         | Alyssa Milano         |
| Milo Yiannopoulos  | Linda Sarsour         |
| Ali Beth Stucky    | David Hogg            |
| Ben Shapiro        | Michael Avenatti      |
| Cape Donktum       | Al Sharpton           |
| Candace Owens      | Louis Farrakan        |
| Stephen Crowder    | Michael Moore         |

## The results and discussion

What follows are all of the results. Each is organised in the same order as the subjects, following first the right-aligned, and then the left-aligned. 

Let’s do it.

### Paul Joseph Watson @PrisonPlanet

This analysis examines two of Paul’s tweets. One with a link, and one without.

![1557641786951](https://biodagar.github.io/propaganda/images/1557641786951.png)

**Causation statements:** None

**Equation statements:** Russians buying ads = horrendous; Russians buying ads = election meddling; Russians buying ads = threat to democracy; leftists (and boomers) = left-aligned people and all baby boomers hold the same attitude.

**Universal quantifiers:** bans **most** of the people who helped Trump get elected. This is clearly absurd: He can’t know this, but uses the quantifier as a catch-all to make a point.

**VAK-OG components:**  Technically *hur dur* (sometimes also written *hurr durr*) is the **sound** of someone with half a brain. It’s most often used to point out an idiotic claim. See the [urban dictionary]([https://www.urbandictionary.com/define.php?term=hurr%20durr](https://www.urbandictionary.com/define.php?term=hurr durr)) for more info.

**What representational signals do the words imply?** This represents left-aligned people as people with strident views (use of exclamation mark). The use of *hur dur* implies that the left-aligned and baby boomers are stupid or have ‘half a brain’, and make idiotic statements. The use of *hur dur* implies that the idea that a private company can do what it likes is itself an idiotic statement. The construction of the snippet is designed to put those who are politically aligned, and baby boomers, in a position where they are being ridiculed (the tone is sarcastic). 

**What frames of reference do the words imply?** Well, there are various things going on here, each of which need to be presented. 

At the **meta** level, they are:

1. That the Paul’s handle *@PrisonPlanet* is a *pre-frame* that establishes the perspective you ought to expect. That is, that you are not free, that people are working against you, that you will be challenged by the content, and also that your thinking has probably been established to keep you thinking in a particular way. 
2. That Russians are seen as being bad by people who are left-aligned politically
3. That anybody elected to a presidency in the USA requires some kind of help, over and above voters casting votes
4. That people who are politically left-aligned, and people who are baby boomers, deserve to be ridiculed because of the things that they say.
5. That Trump needed help to be elected to a presidency in the USA

At the **meta-meta level**, which is the frame of reference that creates meaning about the first frames, we see that the first frames mean that:

* ‘Russian’ means something that isn’t ‘Russian nationality’, but that ‘Russians’ in the American political context is equivalent of ‘spy’
* ‘Helping’ a person to get elected means being visible and identifiable, perhaps ‘in public’
* That Facebook as a company actively silences people with whom it does not agree
* There is no harm in buying ‘a few’ Facebook ads; potentially that ‘a few’ ads is not the same thing as a ‘campaign’ of ads
* People who are left-aligned, and people who are baby boomers, are extreme, reactive, inconsistent people who are idiots that look after their own interests.

At a **meta-meta-meta level**, the ridicule is designed to help you understand that being left-aligned is for stupid, reactionary people who are only interested in themselves. If you were to identify as left-aligned, you may be inclined to either *react* to this in a negative way, thus effectively proving him to be right; or you will feel enough discomfort that you will begin to question your own beliefs and assumptions. If you are still paying attention to him, then you may start to realign your beliefs and assumptions as a result of them getting dislodged by that discomfort.

**What is excluded:** It excludes the information that Paul Joseph Watson was himself banned by Facebook, which is the context for the tweet. The statement excludes an attempt at writing the perspective anybody who is not left-aligned (which doesn’t necessarily just mean right-aligned people). It excludes consideration of people banned by Facebook who are not left-aligned but who may have helped Trump to be elected. It excludes any data about who has been banned by Facebook, which prevents readers from making a choice about their views.

**What operational beliefs appear to be driving the assumptions?** The key belief is that he himself is not left-aligned, as the ridicule is specifically targeted to those who are. In representing two different reactions to a situation involving a common thing (Facebook), one operational belief is that the expressions of a collective of people—who are different to himself—means they must be left-aligned or baby boomers, but that baby boomers will only react when their own interests are involved (company ownership). A third operational belief is that he, and others like him, who identify as Trump supporters, are (or will be) banned by Facebook. A fourth, potential operational belief, is that he himself was one of those who helped Trump to be elected.

**For this to make sense, what must Paul Joseph Watson assume to be real?** That the political alignment of companies’ staff members results in specific outward behaviours of companies (and that, in reverse, a behaviour, action or outcome is an indication of the political beliefs of its staff). That all baby boomers look only after their own interests. That a political alignment is related to one’s intelligence level. That there is a hidden political war that favours those he categorises as Left. That the process of voting is not what elevates someone into the presidency.

![1557647164855](https://biodagar.github.io/propaganda/images/1557647164855.png)

**Causation statements:** None

**Equation statements:** ‘as a’… The entire population that is politically aligned with the left considers this woman to be a ‘social justice hero’.

**Universal quantifiers:** None, unless you consider ‘the left’ to be a universal quantifier for all people who are anti-Trump.

**VAK-OG components:**  None

**What representational signals do the words imply?** The absence of any of the statements or components is itself a representational signal. It indicates an attempt at being impartial: The inclusion of VAK-OG components tend to inspire emotional reactions.

**What frames of reference do the words imply?** The frames of reference at work here are, initially, those that help you to make sense of the action or behaviour. In this case, the action or behaviour was this woman conning money out of Trump supporters.

At the **meta** level, the frames of reference are:

1. That those on the left don’t support Trump
2. That people who are left-aligned see Trump supporters as being racist
3. That this was covered by an independent source (authority: Halo effect)
4. That ‘social justice’ includes feminism, which we get from the ‘stunning and brave’ sarcasm in the construction.

At the **meta-meta level**, which is the frame of reference that creates meaning about the first frames, we see that the first frames mean that:

- There is a common or dominant narrative about Trump supporters shared by people who are politically left-aligned
- That the left-aligned people a a collective believes that supporting its own ideology is preferential to supporting the laws of the country
- That you as a reader are sufficiently well versed in public discourse to understand the commentary
- That you as a reader understand the cultural conversation well enough to understand the sarcasm
- That you as a reader understand this tweet to be a News Style tweet: That it isn’t designed to do more than comment on an event that only just (or just recently) occurred

At a **meta-meta-meta level**, the item could be identified as an attempt to reframe an experience. By pointing out the flaws in the ‘Left’s narrative by pointing to the criminality and logic, it may challenge a left-aligned person’s self-belief about intelligence. That is: It isn’t *social justice* but *criminality*. However, notice that the construction of the sarcasm may also reinforce the division by causing a left-aligned reader to dig more deeply into his or her beliefs as a result of a direct challenge to how they understand themselves. Thus, this piece actually works to reinforce both the position of the writer, and the division that exists between the perceived Left and Right.

If instead the the item in question had followed the logic with a question, it could have formed part of a real attempt to shift someone’s thinking. Instead, it serves to reinforce someone’s thinking about where they are sitting on a political spectrum.

In this way, you can start to see the unyielding, apparently impartial logic to have a specific belief underpinning it, and that this belief is shaping the form of the narrative—thus how you as a reader would approach and react to the piece. 

Of interest, you could also argue that the image, which you will look at *first* because our eyes are drawn to faces, acts psychologically to establish a pre-frame. Depending on your cultural conditioning, you will then allocate that image (black woman with glass) to a particular category.  The text then tells you that this particular woman is stupid, which, given she ran a successful con, is unlikely. She is more likely to be socially inept, because she bragged about it; but that’s a different thing.

**What is excluded:** This narrative excludes any further information about the story—for example, the information that the money was refunded; it excludes information about the definition of criminality in relation to scams in the USA (which maybe different in different countries, especially in relation to the subsequent return of funds to their owners); it excludes further information about the subject of the tweet; it excludes those who may Left-aligned but didn’t share the view of the subject as a ‘social justice hero’; any reader who is not a native English speaker and who thus won’t understand nuances like sarcasm; 

**What operational beliefs appear to be driving the assumptions?** 

Some of the operational beliefs driving the assumptions appear to be:

* Political alignment is an indication of intellect (Left = idiots, stupid; Right = smart, capable of logical thought); potentially also the reverse: That if you are stupid you are probably on the Left.

* That those who read this information (a) see InfoWars as a reliable and valid source of information; (b) are in the USA and/or understand the law of the USA; (c) are well versed in the cultural context; (d) know what sarcasm is and how to read it; (e) are of the same view as the writer; and (f) are likely to see what the writer posts as True.

**For this to make sense, what must Paul Joseph Watson assume to be real?** At a philosophical level, Paul assumes that political alignments are real in the sense that they can be used to categorise humans in a meaningful way. He uses this himself, by further categorising humans into smart or stupid based on their political alignments. 

#### Paul Joseph Watson: In summary

Paul Joseph Watson’s tweets indicate that he has a fixed belief about the role of political alignment as it relates to humans’ experiences of the world and that it *underpins* how smart someone is. His reliance on categorisation (smart/stupid), which is an us/them construction and effectively forces people to pick a side on the basis of how they see themselves intellectually. As a propagandistic tool, look to the neuropsychology of rejection: [‘Rejection piggybacks on physical pain pathways in the brain’](https://www.psychologytoday.com/us/blog/the-squeaky-wheel/201307/10-surprising-facts-about-rejection). If someone would like to be considered to be intelligent, capable of rational and logical thought, then clearly they wouldn’t be aligned with the Left. Would they?

### Meta Propaganda Patterns in Use

Following is a table that collates the elements of propaganda used by each of the subjects in this study. It enables you to see propaganda as a meta concept, rather than a specific and explicit tool.

| **Tool**                                                     | **Left** | **Right** | **Notes** |
| ------------------------------------------------------------ | -------- | --------- | --------- |
| Information exclusion                                        |          |           |           |
| Reframing (meta-meta-meta) (not this, but this)              |          |           |           |
| Pre-framing                                                  |          |           |           |
| Post-framing                                                 |          |           |           |
| Analogous framing (eg. through story)                        |          |           |           |
| Use of misdirection or replacement (and / but)               |          |           |           |
| Use of questions to force thinking down a particular path    |          |           |           |
| Social exclusion based on intellect                          |          |           |           |
| Specific linguistic construction: VAK-OG elements            |          |           |           |
| Specific linguistic construction: Universal quantifiers      |          |           |           |
| Specific linguistic construction: Equation statements        |          |           |           |
| Specific linguistic construction: Causation statements       |          |           |           |
| Specific linguistic construction: Absence of specific elements listed above (apparent impartiality) |          |           |           |
| Use of other sources to support claims (halo effect: authority) |          |           |           |
| Use of images                                                |          |           |           |