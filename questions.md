[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Elements](http://biodagar.github.io/elements/) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)

# Questions

The **primary goal** of this project is a syntopic understanding of the nature, use, and psychology of [propaganda](http://biodagar.github.io/propaganda/definitions).

Along the way, there are particular questions I’m working to answer. The list below will grow over time:

1. What are the [elements](http://biodagar.github.io/propaganda/elements) of propaganda? And, in particular:
   1. What are the mechanics of propaganda?
2. How many of the known works on the subject of propaganda are themselves works of propaganda? For example:
   1. Does its narrative (in whatever form) follow a doctrine, such as politically right or left?
   2. Does its narrative (in whatever form) speak only of “propaganda” as a tool of ‘evil’ or of ‘misleading’ people?
   3. Does its narrative question dominant patterns of existing thought in order to include all perspectives (a good example might be challenging dominant materialist science perspectives or published ‘knowledge’)?
    4. What other questions do we need to ask in order to determine whether a resource *about* propaganda is *itself* propaganda?
3. How meta does one need to become, in reading through this discipline?
4. How many views of the discipline’s history are there? (e.g. is the Britain-centric perspective of propaganda's history correct, or simply dominant?); and
   1. How important is the history, actually?
5. What roles do nation’s cultural ministries and councils play, such as the British Council or Germany’s Goethe Institute.
6. What role do neurolinguistic programming (NLP) and neurosemantics play in the action of propaganda?
   1. How the models created by Hall and Bodenhamer are regarded by other authorities on the subject
   2. Who has already linked NLP, neurosemantics, and cognitive linguistics to propaganda (or created frameworks using it, or unpacked complete campaigns using these approaches)
   3. Whether or not others have expanded on this work since it was written and, if so, what directions they took. 





<hr />

## Changelog

| Changelog                                                    | Date          | By      |
| ------------------------------------------------------------ | ------------- | ------- |
| Add Question 6 re NLP and neurosemantics                     | 25 April 2019 | Leticia |
| Add dominant goal. Rework questions for clarity.             | 13 April 2019 | Leticia |
| Add numbering. Add subsidiary points to Question 2.          | 12 April 2019 | Leticia |
| Questions pulled from [Objectivity](/posts/01Objectivity) and added. | 11 April 2019 | Leticia |
|                                                              |               |         |
