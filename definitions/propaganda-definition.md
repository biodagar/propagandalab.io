[<< Back to Definitions](http://biodagar.github.io/propaganda/definitions) | [Elements of Propaganda](http://biodagar.github.io/propaganda/elements) | [Posts](http://biodagar.github.io/propaganda/posts/PostsIndex)

# Defining ‘Propaganda’

**This project relies on a definition of *propaganda* that I have created. For you to understand this site and this project correctly, it’s important that you understand what *I* mean when I use the term ‘propaganda’. This page gives you an overview of the term ‘propaganda’ for the purposes of this project.**

My national dictionary - the *Macquarie Australian English* dictionary - defines ‘propaganda’ thusly:

> 1. the systematic propagation of a given doctrine.
> 2. the particular doctrines or principles propagated by an organisation or movement.
> 3. dissemination of ideas, information, or rumour for the purpose of injuring of helping an institution, a cause of a person.
> 4. doctrines, arguments, facts spread by deliberate effort through any medium in order to further one’s cause or to damage an opposing cause.
> 5. a public action or display aimed at furthering or hindering a cause.

The term itself comes to us down the ages from 1622’s *Sacra Congregatio de Propaganda Fide*, which was the title of a committee of cardinals established by Pope Gregory XV for the propagation of the Catholic faith. 

This etymology is written in full in the *Macquarie*’s 7th edition in two volumes, but it maps precisely to the etymology at [Etymology Online](<https://www.etymonline.com/search?q=propaganda>) and also to the definition provided in David Welch’s introduction to the volume he edited, titled [*Propaganda, power and persuasion*](references):

> … the origin of the term can be traced back to the Reformation, when the spiritual and ecclesiastical unity of Europe was shattered and the medieval Roman Catholic Church lost its hold on the northern countries. During the ensuing struggle between the forces of Protestantism and those of the Counter-Reformation, the Roman Catholic Church found itself faced with the problem of maintaining and strengthening its hold in the non-Catholic countries. Pope Gregory XIII established a commission of cardinals charged with spreading Catholicism and regulating ecclesiastical affairs in heathen lands … Pope Gregory XV made this commission permanent as the *Sacra Congregatio de Propaganda Fide* (Holy Congregation for the Propagation of the Faith) charged with the management of foregin missions and financed by a ‘ring tax’ assessed upon each newly appointed cardinal. (Welch, 2015. p4)

One of the [issues](issues) in texts and discussion in this field is that many people see ‘propaganda’ as being a specific action, and a negative one at that. However, doing so fails to acknowledge that ‘propaganda is ethically neutral’. (Welch, 2015, p17)

Propaganda itself is, however, a meta concept. It is comprised of:

* public relations
* psyops
* agitation
* information operations, like influence operations
* information warfare
* cultural operations, like political correctness, misinformation, and disinformation
* controlled narratives
* censorship, either explicit or implicit
* media warfare

And it occurs in every channel you can imagine: Mass media, mass entertainment (books, film, music, theatre, art), social media, education systems, policy creation (in all communities, online and in person; and in all governed organisations from government to sporting associations). When you and your peers are censoring each other, that’s a good indication that the action has been effective.

Propaganda has traditionally been seen as the way to capture ‘hearts and minds’ (Welch, 2015; McFarlane, 2018), but in 2019 this phrase is not helpful. The idea of ‘capturing’ hearts and minds forces you to look at tactical methodology. It is more effective to look at the idea of ‘propaganda’ as a cultural action. This is why the definitions provided by the *Macquarie*, above, are also slightly misleading (in my opinion): They appear to be separate definitions, used in separate instances. They’re not.

Propaganda is a *meta-level concept*.

## In summary:

**Propaganda is a sociocultural meta-level concept, which describes the definition and systematic propagation of a set of doctrines, ideas, or principles of any individual, organisation, or movement (or collection thereof), disseminated through ideas, information, public actions, covert actions, displays, influence, behaviours and commentary (among other things), for the purposes of shaping opinions, perceptions, and ultimately the actions of individuals, in support of (and to further) the purpose of the doctrine(s).**

Wherever you see ‘propaganda’ referred to in the [writings](../posts/PostsIndex) of this project, know that it is used in this way, as a meta-level concept.

A very good illustration of propaganda as meta was produced by [The Epoch Times](<https://www.theepochtimes.com/the-unseen-war-on-your-mind_2484017.html>) as part of its commentary on what it calls a ‘war of perception’:

![](..\images\NYEET20180406A06.jpg)
