[<< Back to Definitions](http://biodagar.github.io/propaganda/definitions) | [Questions](http://biodagar.github.io/propaganda/questions) | [Posts](http://biodagar.github.io/propaganda/posts/PostsIndex)

# Defining ‘Fake News’

**This project does not rely on an assumption that the phrase *fake news* means *news that is fake*. Instead, this project takes the phrase ‘fake news’ to mean a specific collection of media outlets. The actual definition followed in this project is in the concluding remarks.**

For you to understand this, you and and I need to be on the same page. Therefore, I’m going to explain how I achieved this understanding.

First, I invite you to review this screenshot:

![](../images/20190411-TrumpTweets-FakeNews.png)



As I had proposed to a friend many months ago, there are two ways to consider the phrase *fake news*.

The first is the meaning that the news is fake. That is simple, and easy. It allows networks to use the phrase without having to think about it too much, and it allows journalists, corporations and networks to cry foul. The other benefit it has is that most people won’t think about it too much either, so they will simply accept that this thinking is correct. 

However, there are two reasons to be cautious. 

The first is that one of the [elements](elements) of propaganda is that it must square with the everyday realities of the populace. 

The second is that another [element](elements) of propaganda is that it must be simple. 

Given a narrative had already been established that Trump was a bit of a crackpot, and given vast numbers of Western populations still have a sense that their media outlets are truth-tellers, could the most simplistic definition, one that is coherent with the existing narrative, itself be a propagandist’s tool?

Of course it could.

But if so, what else might the phrase mean?

To answer this, we need to examine usage. In almost all cases in which Trump uses the phrase *fake news* it has a particular pattern of capitalisation: Fake News.

This is significant because when you examine Trump’s narratives, you will notice that he uses capitals in a consistent manner. I agree it sometimes looks odd, but I suggest that the oddness is because it (a) runs against other, ‘standard’ usage; and (b) it’s unfamiliar to people who aren’t in the habit of reading older books. 

The consistent pattern of capitalisation tends to follow capitalisation of nouns, though in a specific way. English as a language has evolved past the capitalisation of nouns, such that we only capitalise proper nouns now, and even this is much looser than it was just 40 years ago. The rule that Trump’s tweets follow is the capitalisation of nouns where those nouns refer to something specific (which you often see in books as ‘previously referenced’).

For context, I learned these rules more than 30 years ago. Someone of Trump’s age was learning them perhaps 60 years ago.

Here are some examples:

![](../images/20190411-TrumpTweets-Caps1.png)

![](../images/20190411-TrumpTweets-Caps2.png)

The pattern of capitalisation in Trump’s case doesn’t follow a rule that says ‘capitalise all nouns’. In his case, the capitalisation follows a rule that says ‘capitalise specific references’. 

Therefore, *American Tourist* is a specific American tourist. The *13 Trump Haters & Angry Democrats* refer to a specific group of people within the Democrat political party (which may or may not be its leaders). *Mainstream Media* refers to a specific collection of media outlets, collectively known as ‘mainstream’. And *Fake News Media* is a specific subset of media outlets that may or may not be part of the mainstream. (Though if you look at Trump’s past tweets you’ll see that he collates The Fake News with Mainstream Media.)

This patterning was most evident in early days when *fake news media* was referred to as **the** *fake news media* or **the** *fake news*, because the use of the article ‘the’ also indicates something ‘specific’. The actual rule is that *the* is a definite article.

When you see *The Fake News* you know, therefore, that not only is *Fake News* a name given to something, but that it is a name given to a *definite* something. Simply understanding *fake news* to mean *news that is fake* doesn’t comply with the referential rules that Trump has established in his own usage, and especially not with its definite references.

![](../images/20190411-TrumpTweets-Caps3.png)

The first tweet in this list comes very close to confirming evidence for my analysis of the pattern. *The Fake News is not mentioning…* tells us that ‘The Fake News’ refers to a definite and specific group of news outlets; it is not that ‘news mentions are fake’. Same with ‘made up stories by The Fake News’, and ‘The Fake News Media’.

**Your takeaway from this** is that in my study on propaganda, the phrase ‘Fake News’ has a specific definition. That definition is that it is *A collective of media and news outlets that present a particular perspective in order to support a particular doctrine for the purposes of public persuasion*. It is not ‘news that is fake’.





<hr />

## Changelog

| Created       | Updated | Changelog   |
| ------------- | ------- | ----------- |
| 12 April 2019 |         | Create file |
|               |         |             |
