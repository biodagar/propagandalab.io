[<< Back to Home](http://biodagar.github.io/propaganda/posts/Posts-00Index) | [Questions](http://biodagar.github.io/propaganda/questions) | [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)

# Definitions

Key terms in this project are defined by yours truly. To properly understand the (present and future) writing here, you’ll need to be *au fait* with the terminology.

Terms may be listed here, but not linked, if the pages are not yet complete.

<hr />

1. [Propaganda](http://biodagar.github.io/propaganda/definitions/propaganda-definition)
2. [Fake News](http://biodagar.github.io/propaganda/definitions/FakeNews)