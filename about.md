[<< Back to Home](http://biodagar.github.io/propaganda/) | [Definitions](http://biodagar.github.io/propaganda/definitions) | [Questions](http://biodagar.github.io/propaganda/questions) | [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)

# About: More detail

The project is a personal one: It's not academic, or business-oriented. My objective is to publish a book. I have an idea of what that looks like, but no further details are available to you right now. :)

#### If you have a vested interest in this topic, I want to hear from you!
This is particularly the case if you are writing in, on, or around the field. See my contact details (below) to share ideas, links, suggestions, papers or works you think ought to be included.

### This site is part of my commitment to transparent writing practice
Most of the time, I do this by way of a [Patreon community](https://patreon.com/biodagar), which gives supporters meaningful insights and publications. However, this site goes one step further, being a place for my 'writing through' my study. Think of it like an open writing journal. Follow along at your leisure.

### Get updates about this project
You can add yourself to [the Propaganda distribution list](https://send.zingry.com/h/i/24E55148C1F6AEDF) if you'd like to hear from me occasionally about the project and its development. It won't be very often. :)

### Contact Me
Send an email to biodagar [at] biodagar [dot] com, or send snail mail, postcards, journal papers, or donated books to Propaganda, PO Box 1190, Pasadena SA 5042 Australia.

You can also Tweet me [@biodagar](https://twitter.com/biodagar).
