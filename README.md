# Home

This is the open website for a personal project of mine, which is to trace and document my studies of propaganda. [Learn more about why this project exists](https://biodagar.github.io/propaganda/about).

## This site is changing only slowly. Here’s why

Initially, in a phase of enthusiastic ignorance, I had the idea that I could write and publish as I went. That I could publish here, and that it would be ok.

That’s not the case.

In order to move this project along more quickly, I’ve had to define a research strategy, and that strategy sees focused research occur long before more writing does.

The work is being captured in TheBrain, which I will make available to you as soon as it’s in a state fit to for sharing. And that means: When it’s closer to completion.

**I’ve left the posts up here** as a taster for you.

### The ongoing thinking about this project is at SubscribeStar

The project is so big, and touches so many elements of life, that it won’t be possible to do it in isolation. SubscribeStar allows me to share the journey with others and, in turn, to be influenced by those people. 

**[You can join in by starting with my SubscribeStar page, here](https://www.subscribestar.com/leticiamooney)**

## Sitemap

Many of these pages are dormant as of 23 July 2019. Subscribe to the [Propaganda distribution list](https://send.zingry.com/h/i/24E55148C1F6AEDF) or join the [SubscribeStar](https://www.subscribestar.com/leticiamooney) community for updates.

* [About](http://biodagar.github.io/propaganda/about)
* [Definitions and Terminology](http://biodagar.github.io/propaganda/definitions)
  * [Fake News](http://biodagar.github.io/propaganda/definitions/FakeNews)
  * [Propaganda](http://biodagar.github.io/propaganda/definitions/propaganda-definition)
* [Elements of propaganda](http://biodagar.github.io/propaganda/elements)
* [Issues](http://biodagar.github.io/propaganda/issues)
* [Posts](http://biodagar.github.io/propaganda/posts/Posts-00Index)
  * [Objectivity](http://biodagar.github.io/propaganda/posts/objectivity)
  * [Propaganda Studies as Propaganda](http://biodagar.github.io/propaganda/posts/PropagandaStudies)
  * [Propaganda is Programming](https://biodagar.github.io/propaganda/posts/PropagandaLinguistics)
* [Questions](http://biodagar.github.io/propaganda/questions)



## Latest Updates
**Date: 23 July 2019**

* Remove the Bibliography link
* Add section on why this site is changing slowly
* Add note that the pages are dormant effective 23 July 2019
